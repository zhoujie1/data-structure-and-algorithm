/**
 * 赫夫曼树节点对象
 */
public class HfTreeNode {

    /**
     * 左子节点
     */
    private HfTreeNode leftNode;
    /**
     * 右子节点
     */
    private HfTreeNode rightNode;
    /**
     * 节点数据
     */
    private String data;
    /**
     * 出现次数
     */
    private int count;

    /**
     * 是否是数据，true表示数据，false -- data数据为权重节点
     */
    private boolean isData;
    public HfTreeNode getLeftNode() {
        return leftNode;
    }

    public void setLeftNode(HfTreeNode leftNode) {
        this.leftNode = leftNode;
    }

    public HfTreeNode getRightNode() {
        return rightNode;
    }

    public void setRightNode(HfTreeNode rightNode) {
        this.rightNode = rightNode;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public boolean isData() {
        return isData;
    }

    public void setData(boolean data) {
        isData = data;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
