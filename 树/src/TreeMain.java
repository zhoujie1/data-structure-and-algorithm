import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TreeMain {
    public static void main(String[] args) {
        List<String> strings =  Arrays.asList("A", "B",null, "D",null,null,"C",null, "E",null,null);
        /**
         * 这里多做了一步操作，不是我吃饱了撑得哈,asList在使用remove方法的时候，会抛出 UnsupportedOperationException异常。
         */
        List<String> data = new ArrayList<>(strings);
        BiTreeNode treeByFront = createTreeByFront(data);
        /**
         * 中序遍历
         */
        getOrderByMid(treeByFront);
    }

    /**
     * 前序遍历创建树
     * @param strings
     * @return
     */
    public static BiTreeNode  createTreeByFront(List<String>  strings){
        if (strings.isEmpty()){
            return null;
        }else {
            BiTreeNode biTreeNode = new BiTreeNode();
            String s = strings.get(0);
            /**
             * 字符串为null，该节点不存在
             */
            if (s == null){
                strings.remove(0);
                return null;
            }else {
                /**
                 * 设置节点值
                 */
                biTreeNode.setData(s);
                /**
                 * 移除第一个节点，如果包含大量节点，可以考虑linkedList,这里只为实现功能以及原理
                 */
                strings.remove(0);
                /**
                 * 设置左右字节点
                 */
                biTreeNode.setLeftNode(createTreeByFront(strings));
                biTreeNode.setRightNode(createTreeByFront(strings));
                return biTreeNode;
            }
        }
    }

    /**
     * 中序遍历
     */
    public static  void getOrderByMid(BiTreeNode node){
        if (node == null){
            return;
        }else {
            /**
             * 遍历左子树
             */
            getOrderByMid(node.getLeftNode());
            System.out.println(node.getData());
            /**
             * 遍历右子树
             */
            getOrderByMid(node.getRightNode());
        }
    }
}
