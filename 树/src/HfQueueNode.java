public class HfQueueNode {
    /**
     * 具体数据（字符串）
     */
    private String data;
    /**
     * 以出现次数代表权重
     */
    private int count;
    /**
     * 是否是数据
     */
    private boolean isData;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public boolean isData() {
        return isData;
    }

    public void setIsData(boolean data) {
        isData = data;
    }
}
