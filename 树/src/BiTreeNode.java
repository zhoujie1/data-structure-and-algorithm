/**
 * 二叉树节点
 */
public class BiTreeNode {

    /**
     * 左子节点
     */
    private BiTreeNode leftNode;
    /**
     * 右子节点
     */
    private BiTreeNode rightNode;
    /**
     * 节点数据
     */
    private String data;

    public BiTreeNode getLeftNode() {
        return leftNode;
    }

    public void setLeftNode(BiTreeNode leftNode) {
        this.leftNode = leftNode;
    }

    public BiTreeNode getRightNode() {
        return rightNode;
    }

    public void setRightNode(BiTreeNode rightNode) {
        this.rightNode = rightNode;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
