public class ZQueueMain {
    public static void main(String[] args) {
        ZQueue zQueue = new ZQueue();
        zQueue.add("1");
        zQueue.add("2");
        zQueue.delete();
        zQueue.add("3");
        zQueue.add("4");
        zQueue.add("5");
        zQueue.add("6");
        Object delete = zQueue.delete();
        zQueue.add("7");
        System.out.println(delete);

    }
}
