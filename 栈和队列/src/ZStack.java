public class ZStack {
    /**
     * 栈顶指向
     */
    private int top = 0;
    /**
     * 栈底指向
     */
    private int bottom = 0;
    /**
     * 栈长度
     */
    public int length = 0;
    /**
     * 栈内数据
     */
    private Object[] datas;

    /**
     * 栈内最大空间
     */
    public int MAX_SIZE = 100;

    /**
     * 出栈
     *
     * @return
     */
    public Object pop() {
        // 栈顶与栈底指向同一个位置
        if (top == 0) {
            return null;
        } else {
            // 栈顶下移
            top--;
            // 取出栈顶数据
            Object data = datas[top];
            // 栈顶置为空
            datas[top] = null;
            length--;
            return data;
        }
    }

    /**
     * 入栈
     *
     * @param o
     */
    public void push(Object o) {
        if (top < MAX_SIZE){
            datas[top] = o;
            top++;
            length++;
        }else {
            throw new OutOfMemoryError("栈已满，不能再加了");
        }

    }

    // 初始化栈
    public ZStack() {
        // 默认栈长度为100
        datas = new Object[MAX_SIZE];
    }
}
