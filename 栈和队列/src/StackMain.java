import java.util.Scanner;

public class StackMain {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        /**
         * 十进制转二进制
         */
        // dem2Binary(Integer.parseInt(scanner.next()));
        // rpn(scanner.next());
        // 中缀转后缀表达式
        String parseSuffixExpression = parseSuffixExpression(scanner.next());
        // 逆波兰表达式计算
        rpn(parseSuffixExpression);

    }

    /**
     * 中缀转后缀表达式
     * @param next
     * @return
     */
    private static String parseSuffixExpression(String next) {
        StringBuffer suffixSb = new StringBuffer();
        char[] chars = next.toCharArray();
        ZStack zStack = new ZStack();
        for (int i = 0; i < chars.length; i++) {
            char thisChar = chars[i];
            // 判断字符是否是数字，如果是数字，直接输出
            if (Character.isDigit(thisChar)) {
                suffixSb.append(thisChar);
            } else if (thisChar == ')') {
                /**
                 * 当入栈时是）时，栈为空时依次出栈，栈不为空依次出栈，知道遇到（停止
                 */
                // 栈为空
                if (zStack.length == 0) {
                    zStack.push(thisChar);
                } else {
                    Object pop = zStack.pop();
                    while (pop != null && !String.valueOf(pop).equals("(")) {
                        suffixSb.append(pop);
                        pop = zStack.pop();
                    }
                }
            } else if (thisChar == '+' || thisChar == '-') {
                /**
                 * 当为+，或者-入栈时，需要与栈顶元素的优先级比较，优先级高的需要先出栈，直到遇到（或者栈为空
                 */
                // 栈为空，直接push
                if (zStack.length == 0) {
                    zStack.push(thisChar);
                } else {

                    Object pop = zStack.pop();
                    while (pop!=null && !String.valueOf(pop).equals("(")){
                            suffixSb.append(pop);
                        pop = zStack.pop();
                    }
                    /**
                     * （出栈了，需要重新入栈，重点
                     */
                    if (String.valueOf(pop).equals("(")){
                        zStack.push("(");
                    }
                    zStack.push(thisChar);
                }
            } else if (thisChar == '*' || thisChar == '/' || thisChar == '(') {
                /**
                 * 为* ，/，（ 直接入栈
                 */
                zStack.push(thisChar);
            } else {
                System.err.println("输入错误");
            }
        }
        /**
         * 输出栈内其它元素
         */
        while (zStack.length!=0) {
            suffixSb.append(zStack.pop());
        }

        return suffixSb.toString();
    }

    /**
     * 逆波兰表达式计算器，输入的是一个波兰表达式
     * @param next
     */
    private static void rpn(String next) {
        if (!next.isEmpty()) {
            ZStack zStack = new ZStack();
            char[] chars = next.toCharArray();
            for (int i = 0; i < chars.length; i++) {
                char thisChar = chars[i];
                // 判断字符是否是数字，如果是数字，就入栈
                if (Character.isDigit(thisChar)) {
                    zStack.push(thisChar);
                } else {
                    // 先出的是后面的操作数
                    Double behind = Double.parseDouble(String.valueOf(zStack.pop()));
                    Double front = Double.parseDouble(String.valueOf(zStack.pop()));
                    switch (thisChar) {
                        case '+':
                            zStack.push(front + behind);
                            break;
                        case '-':
                            zStack.push(front - behind);
                            break;
                        case '*':
                            zStack.push(front * behind);
                            break;
                        case '/':
                            if (behind == 0) {
                                throw new NumberFormatException("被除数不能为0");
                            }
                            zStack.push(front / behind);
                            break;
                        default:
                            break;
                    }
                }
            }
            System.out.println(next + "计算结果为：" + zStack.pop());

        }
    }


    /**
     * 十进制转2进制
     *
     * @param num
     */
    static void dem2Binary(int num) {
        int temNum = num;
        // 初始化栈
        ZStack zStack = new ZStack();
        while (num > 0) {
            // 余数入栈
            zStack.push(num % 2);
            num = num / 2;
            if (num == 1) {
                zStack.push(num);
                num = 0;
            }
        }
        // 出栈，打印出二进制内容
        StringBuffer sb = new StringBuffer();
        while (zStack.length > 0) {
            sb.append(zStack.pop());
        }
        System.out.println("十进制：" + temNum + "转为二进制为:" + sb.toString());
    }

}
