/**
 * 队列类实现(顺序存储实现)
 */
public class ZQueue {
    // 当前队列长度
    private int length = 0;
    // 队列最大长度
    private int MAX_SIZE = 5;
    // 队列数据
    private Object[] datas;

    /**
     * 队头索引
     */
    private int head = 0;
    /**
     * 队尾索引
     */
    private int tail = 0;

    /**
     * 出队列操作
     *
     * @return
     */
    public Object delete() {
        Object returnObj = new Object();
        if (head == tail) {
            if (datas[head] == null) {
                System.err.println("队列已空，不能出队列");
                ;
            } else {
                returnObj = datas[head];
                datas[head] = null;
            }
        } else {
            /**
             * 队列头置为空，将头往后移
             */
            returnObj = datas[head];
            datas[head] = null;
            head = (head + 1) % MAX_SIZE;
        }
        length--;
        return returnObj;
    }

    /**
     * 入队列操作
     */
    public void add(Object obj) {
        if (length == MAX_SIZE) {
            System.err.println("队列已满，不能入队");
        } else {
            // 队尾累加，如果到顶了，头尾相接，再从头开始，形成一个循环队列
            tail = length == 0 ? tail : (tail + 1) % MAX_SIZE;
            datas[tail] = obj;
            length++;
        }
    }

    public ZQueue() {
        this.datas = new Object[MAX_SIZE];
    }
}
