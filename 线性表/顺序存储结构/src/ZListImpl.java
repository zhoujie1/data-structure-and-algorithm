public class ZListImpl implements ZList {
    // 数据集合
    private ZElement[] datas;

    // 数组最大长度
    private final int maxLength;

    /**
     * 数组初始长度为0
     */
    private int length = 0;

    public ZListImpl(int maxLength) {
        this.maxLength = maxLength;
        this.datas = new ZElement[maxLength];
    }


    @Override
    public void delete(int index) {
        if (index < 0 || index > length - 1) {
            System.err.println("index越界");
        }
        // 当前索引位置数据后面的往前挪一位
        for (int i = index; i < length; i++) {
            datas[i] = datas[i+1];
        }
        //数组长度减一
        length--;
    }

    @Override
    public void add(ZElement zElement) {
        // 不允许插入空值
        if (zElement.getData() == null) {
            return;
        }
        // 当前数组长度大于最大长度，暂不支持动态扩容
        if (length >= maxLength) {
            System.err.println("该链表数据已满");
        }
        datas[length] = zElement;
        // 数组长度增加
        length++;
    }

    @Override
    public int getId(ZElement element) {
        // -1 表示不存在该元素
        if (element.getData() == null) {
            return -1;
        }
        // 如果有多个元素匹配，返回第一个索引值
        for (int i = 0; i < length; i++) {
            if (datas[i].getData().equals(element.getData())) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int length() {
        return length;
    }
}
