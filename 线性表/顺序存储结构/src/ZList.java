import java.util.List;

public interface ZList {
    /**
     * 删除元素 通过id或者元素值删除
     * @param
     */
    void delete(int id);

    /**
     * 添加元素
     * @param zElement
     */
    void add(ZElement zElement);

    /**
     * 返回元素索引，及查找
     * @param element
     * @return
     */
    int getId(ZElement element);

    /**
     * 查询元素长度
     * @return
     */
    int length();

}
