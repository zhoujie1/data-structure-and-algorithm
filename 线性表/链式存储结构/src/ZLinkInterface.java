public interface ZLinkInterface {
    // 在某个数据后新增一个节点
   void add(String target,String data);

   // 删除节点
    void delete(String data);

    // 查询节点
    ZNode get(String data);
}
