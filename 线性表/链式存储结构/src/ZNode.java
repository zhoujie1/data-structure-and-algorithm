public class ZNode {
    // 下一个节点
    private ZNode next = null;
    // 当前节点数据
    private String data;

    public ZNode getNext() {
        return next;
    }

    public void setNext(ZNode next) {
        this.next = next;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public ZNode(String data) {
        this.data = data;
    }
}
