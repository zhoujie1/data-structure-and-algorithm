import javax.xml.soap.Node;

public class ZLinkImpl implements ZLinkInterface {
    // 初始化头节点
    ZNode head = null;
    // 链表长度
    public int size = 0;

    @Override
    public void add(String target, String data) {
        ZNode targetNode = get(target);
        if (targetNode == null) {
            add(data);
        } else {
            ZNode zNode = new ZNode(data);
            zNode.setNext(targetNode.getNext());
            targetNode.setNext(zNode);
            size++;
        }
    }

    @Override
    public void delete(String data) {
        ZNode pNode = getPreNode(data);
        ZNode next = pNode.getNext();
        // 不是最后一个节点
        if (next != null) {
            // 上一节点数据指向当前节点的下一个节点
            pNode.setNext(next.getNext());
        } else {
            pNode.setNext(null);
        }
        this.size--;

    }

    @Override
    public ZNode get(String data) {
        if (this.size != 0 && data != null) {
            ZNode next = head;
            while (next != null) {
                if (data.equals(next.getData())) {
                    return next;
                } else {
                    next = next.getNext();
                }
            }
        }
        return null;
    }

    public ZNode getPreNode(String data) {
        if (this.size != 0 && data != null) {
            ZNode next = head.getNext();
            while (next != null) {
                if (data.equals(next.getNext().getData())) {
                    return next;
                } else {
                    next = next.getNext();
                }
            }
        }
        return null;
    }

    // 没有目标，采用尾插法插入到链表最后
    public void add(String data) {
        // 当前链表为空
        if (this.size == 0) {
            head = new ZNode(data);
        } else {
            // 当前链表游标，获取链表最后游标
            ZNode thisCur = head;
            while (thisCur != null && thisCur.getNext() != null) {
                thisCur = thisCur.getNext();
            }
            // 待插入链表插入到当前节点后面
            ZNode willNode = new ZNode(data);
            thisCur.setNext(willNode);
        }
        size++;
    }
}
